# test-cliente-php-api

Carles Miranda

# Installation
Clone this project in your work folder and open the directory:

```bash
    git clone https://gitlab.com/carles.mr.19/test-cliente-php-api.git
```

#### Open the folder "test-cliente-php-api"

```bash
    cd test-cliente-php-api
```

#### Start the json-server

```bash
    json-server --watch db.json
```


Finally we will have to enter the "localhost/test-cliente-php-api/public" address in our browser to see the application.


---

Carles Miranda Rodriguez

[Linkedin](https://www.linkedin.com/in/carles-miranda/)

---




