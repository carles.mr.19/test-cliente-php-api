<?php
    include('Api.php');

    $api = new Api();
    $datos = $api->listar();
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Carles Miranda</title>

        <meta charset="UTF-8">
		
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
            <hr>           
                <button type="button" class="btn btn-primary float-right" data-toggle="modal" data-target="#exampleModal"> Añadir Persona </button>
                <h3 class="text-center">Carles Miranda</h3>
            <hr>       
            <table class="table table-bordered table-striped">
					<thead>
						<tr>
                            <th>ID</th>
							<th>Nombre</th>
                            <th>Apellido</th>
                            <th>Accion</th>
					    </tr>
				    </thead>
			    <tbody>
                <?php 

                    if(count($datos) > 0){

                    
                    foreach($datos as $key=> $value) {
                       $id = $value["id"];
                        $nombre = $value["nombre"];
                        $apellido = $value["apellido"];

                        echo "
                        <tr>
                            <td>$id</td>
                            <td>$nombre</td>
                            <td>$apellido</td>
                            <td>
                                <form action='show.php' method='POST' class='float-left'>
                                    <input id='idUser' name='idUser' type='hidden' value='".$id."' >
                                    <button type='submit' class='btn btn-primary btn-sm'>Mostrar</button>
                                </form>
                            
                            <form action='edit.php' method='POST' class='float-left ml-1'>
                                <input id='idUser' name='idUser' type='hidden' value='".$id."' >
                                <button type='submit' class='btn btn-warning btn-sm'>Editar</button>
                            </form>
                            
                            <form action='Api.php' method='POST' class='float-left ml-1'>
                                <input id='idUser' name='idUser' type='hidden' value='".$id."'>
                                <button type='submit' class='btn btn-danger btn-sm' name='eliminar'>Eliminar</button>
                            </form>
                            </td>
                        </tr>";
                    }
                }else{
                    echo "<tr><td colspan='4' class='text-center'> No existen datos</td></tr>";
                }

                ?>
                </tbody>
			</table>
		</div>
	</body>
</html>

<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
    
    <form action="Api.php" method='POST'>
        <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Añadir Persona</h5>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body">
            <div class="form-group">
                <label for="nombre">Nombre</label>
                <input type="text" class="form-control" id="nombre" name="nombre" placeholder="Introduze un nombre" required>
            </div>
            <div class="form-group">
                <label for="Apellido">Apellido</label>
                <input type="text" class="form-control" id="apellido" name="apellido" placeholder="Introduze un apellido" required>
            </div>
        </div>
        <div class="modal-footer">
            <button type="submit" class="btn btn-primary" name="crear"> Añadir</button>
            
        </div>
      </form>
    </div>
  </div>
</div>