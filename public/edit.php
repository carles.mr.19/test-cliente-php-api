<?php
    include('Api.php');

    $api = new Api();
    $datos = $api->getData($_POST["idUser"]);
?>

<!DOCTYPE html>
<html>
	<head>
		<title>Carles Miranda</title>

        <meta charset="UTF-8">

		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
        <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
	</head>
	<body>
		<div class="container">
            <h3 align="center">Carles Miranda</h3>
            
            <form action="Api.php" method='POST'>
                <div class="form-group">
                    <label for="nombre">Nombre</label>
                    <input type="text" class="form-control" id="nombre" name="nombre" value="<?php echo $datos["nombre"]; ?>">
                </div>
                <div class="form-group">
                    <label for="Apellido">Apellido</label>
                    <input type="text" class="form-control" id="apellido" name="apellido" value="<?php echo $datos["apellido"]; ?>">
                </div>

                <input id='idUser' name='idUser' type='hidden' value='<?php echo $datos["id"]; ?>'>
                <button type='submit' class='btn btn-success' name="modificar">Actualizar</button>
            </form>
		</div>
	</body>
</html>