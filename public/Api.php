<?php

class Api {

    var $url = "http://localhost:3000/";

    public function url($ruta){
        $res = $this->url . $ruta;
        return $res;
    }

    public function listar(){
        $dir = $this->url("usuarios");    
        $json = file_get_contents($dir);
        $datos = json_decode($json, true);

        return $datos;
    }   
   
    public function crear(){
        $dir = $this->url("usuarios");

        $jsonData = array(
            "nombre"=> $_POST['nombre'],
            "apellido"=> $_POST['apellido']
        );

        $jsonDataEncoded = json_encode($jsonData);

        $ch = curl_init($dir);

        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

        if(curl_exec($ch)){
            header('Location: index.php');
            exit;
        }
    }

    public function getData($id){
        $dir = $this->url("usuarios/$id");

        $json = file_get_contents($dir);
        $datos = json_decode($json, true);

        return $datos;
    }

    public function modificar($data, $id){
        $dir = $this->url("usuarios/$id");

        $ch = curl_init($dir);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS,http_build_query($data));

        if(curl_exec($ch)){
            header('Location: index.php');
            exit;
        }
    }

    public function eliminar($id){
        $dir = $this->url("usuarios/$id");

        $ch = curl_init($dir);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");

        if(curl_exec($ch)){
            header('Location: index.php');
            exit;
        }
    }

    public function volver(){
        header('Location: index.php');
        exit;
    }
} 

    $api = new Api();

    if (isset($_POST['crear'])) {
        $api->crear();
    }

    if(isset($_POST['eliminar'])){
        $api->eliminar($_POST['idUser']);
    }

    if(isset($_POST['mostrar'])){
        $api->mostrar($_POST['idUser']);
    }

    if(isset($_POST['volver'])){
        $api->volver();
    }

    if(isset($_POST['modificar'])){
        $data = array(  
            "id" => $_POST["idUser"],
            "nombre" => $_POST["nombre"],
            "apellido" => $_POST["apellido"],
        );

        $api->modificar($data, $_POST['idUser']);
    }
    
?>
